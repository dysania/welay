# build for use with podman

FROM docker://ubuntu:rolling

WORKDIR /root

RUN apt-get update && apt-get -qqy install curl sudo byobu
ADD scripts/install-dev-tools.sh /opt/install-dev-tools.sh

RUN chmod +x /opt/install-dev-tools.sh
RUN ["bash", "-c", "/opt/install-dev-tools.sh 2>&1 > /dev/null"]
