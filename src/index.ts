import bodyParser from "body-parser";
import { exec } from "child_process";
import express from "express";
import { statSync } from "fs";
import helmet from "helmet";

const app = express();
const DEFAULT_PORT = 8080;

app.use(helmet());
app.use(bodyParser.json());
app.use((req, res: any, next) => {
  res._startTime = Date.now();
  next();
});

app.use((req, res, next) => {
  if (req.body.secret !== process.env.WELAY_SECRET) {
    return end(req, res, 403);
  }

  next();
});

app.post("/:script([\\w-]+.sh)(/*)?", (req, res) => {
  try {
    statSync(req.params.script);
  } catch (e) {
    console.error(e.message);
    return end(req, res, 404);
  }

  exec(`./${req.params.script}`, (err, stdOut, stdErr) => {
    if (err) {
      console.error(err.message);
      return end(req, res, 500);
    }

    end(req, res, 200);
  });
});

// catch-all
app.use((req, res) => {
  end(req, res, 404);
});

function end(req: express.Request, res: any, status: number) {
  const now = new Date();
  res.status(status).end();

  console.log(
    req.originalUrl,
    res.statusCode,
    `${now.getTime() - res._startTime}ms`,
    now.toISOString()
  );
}

const server = app.listen(process.env.PORT || DEFAULT_PORT, () => {
  let addressInfo = server.address();
  let address: string;

  if (addressInfo === null) {
    addressInfo = "<unknown>";
  }

  if (typeof addressInfo === "string") {
    address = addressInfo;
  } else {
    address = `localhost:${addressInfo.port}`;
  }

  console.log(`=> Server listening at http://${address}`);
});
