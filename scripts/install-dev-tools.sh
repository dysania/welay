#!/bin/bash

# This assumes you're on Ubuntu!

set -e

# get package managed tools
# - build-essential: has our build tool `make` and other dependecies for things like node-gyp
# - curl: to install nvm
sudo apt-get install -qqy build-essential curl

# get nvm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash

# install node
source ~/.nvm/nvm.sh
nvm install --no-progress --lts node
